## 技术栈

- vue3.2
- typescript
- pinia
- vue-router
- tailwindcss
- daisyUI / headlessUI/Vue
- vitest

## src目录构成

- types: ts 类型
- store: 全局数据仓库
- router: 路由信息
- utils: 工具类
- api: 发起网络请求
