export type userInfoType = {
    name: string,
    job: string,
    email: string,
    phone: string,
    wechat: string,
    blogUrl: string,
    repositoryUrl: string
}

export type userInfo = {
    username: string,
    password: string,
};
